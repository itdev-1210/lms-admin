const rowStyle = {
  width: '100%',
  display: 'flex',
  flexFlow: 'row wrap',
};
const colStyle = {
  marginBottom: '16px',
};
const customCol = {
  marginBottom: '16px',
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'center',
  marginRight: '30px'
}
const gutter = 16;
const basicStyle = {
  rowStyle,
  colStyle,
  customCol,
  gutter,
};

export default basicStyle;
