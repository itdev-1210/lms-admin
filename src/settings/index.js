export default {
  apiUrl: 'http://yoursite.com/api/',
};

const siteConfig = {
  siteName: 'LMS(仮)',
  siteIcon: 'ion-flash',
  footerText: 'LMS(仮) ©2019 Created by SC',
};
const themeConfig = {
  topbar: 'themedefault',
  sidebar: 'themedefault',
  layout: 'themedefault',
  theme: 'themedefault',
};
const language = 'japanese';

const jwtConfig = {
  fetchUrl: 'http://lms-mock.xist.co.jp/api/v1/',
  secretKey: 'secretKey',
};

export { siteConfig, language, themeConfig, jwtConfig };
