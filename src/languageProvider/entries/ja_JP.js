import antdEn from "antd/lib/locale-provider/ja_JP";
import appLocaleData from 'react-intl/locale-data/ja';
import jaMessages from '../locales/ja_JP.json';

const JaLang = {
  messages: {
    ...jaMessages,
  },
  antd: antdEn,
  locale: 'ja-JP',
  data: appLocaleData,
};
export default JaLang;
