import React, { Component } from 'react';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import Box from '../../components/utility/box';
import ContentHolder from '../../components/utility/contentHolder';
import { Row, Col } from 'antd';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';
import Button from '../../components/uielements/button';
import Modal from 'react-responsive-modal';
import notification from '../../components/notification';
import api from '../../redux/api';
import CommentModal from './modal';

import basicStyle from '../../settings/basicStyle';
import './tree.scss'

export default class extends Component {

  constructor(props){
    super(props);
    this.state={
      orgChart: null,
      data: [],
      studentInfo: null,
      modalIsOpen: false,
      selUnit: []
    }
    this.renderChart = this.renderChart.bind(this)
    this.init = this.init.bind(this)
    this.getNodeByClickedBtn = this.getNodeByClickedBtn.bind(this)
    this.renderNodeEventHandler = this.renderNodeEventHandler.bind(this)
    this.commentModal = this.commentModal.bind(this)
    this.setActive = this.setActive.bind(this)
    this.handleClickActive = this.handleClickActive.bind(this)
  }

  commentModal(id) {
    const { data } = this.state
    var sel = data.filter(data => data.id === parseInt(id))
    if (sel.length > 0)
      sel = sel[0]

    this.setState({selUnit: sel, modalIsOpen: true})
  }

  onCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  handleClickActive(id) {
    api.POST('school/unit/set-studying', {
      studentId: this.state.studentId,
      learningUnitId: id
    })
    .then(({data}) => {
      api.GET('school/unit/relation', {
        studentId:this.state.studentId
      })
      .then(({data}) => {
        console.log(data)
        this.setState({data: data.data}, ()=>this.renderChart())
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
  }
  setActive(id) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui'>
            <h2>変更なさいますか?</h2>
            <button onClick={onClose}>{'いいえ'}</button>
            <button className="yes-confirm"
              onClick={() => {
                this.handleClickActive(id);
                onClose();
              }}
            >
              {'はい'}
            </button>
          </div>
        );
      }
    });
  }
  renderChart() {
    var data = this.state.data
    if (data.length < 1)
      return;

    var i = 0, j = 0
    for(i=0; i < data.length; i++) {
      data[i]['progressRate'] = data[i]['progressRate'] + '%'
      data[i]['progress'] = '進捗率'
      data[i].isCurrent = ' '
      if (data[i].isActive)
        data[i].isCurrent = '現在学習中'
      if (data[i]['id'] === data[i]['parentId'])
        data[i]['parentId'] = null
      if (i === data.length - 1)
        break;
      for (j=i+1; j < data.length; j++) {
        if (data[i].id === data[j].id) {
          data[i]['secondParentId'] = data[j].parentId
          data.splice(j, 1)
        }
      }
    }

    var _this = this;
    window.getOrgChart.themes.myTheme =
    {
      size: [330, 350],
      toolbarHeight: 0,
      textPointsNoImage: [
          { x: 20, y: 45, width: 300 },
          { x: 200, y: 85, width: 300 },
          { x: 20, y: 125, width: 300 },
          { x: 200, y: 125, width: 300 },
      ],
      box: '<rect x="0" y="0" height="350" width="330" rx="10" ry="10" class="get-box"></rect>'
          + '<g transform="matrix(1,0,0,1,0,145)" class="btn" data-action="comment"><rect x="15" y="30" height="40" width="300" class="default-btn"></rect><text x="90" y="55" width="200" class="button-text">コメントを表示</text></g>'
          + '<g transform="matrix(1,0,0,1,0,205)" class="btn" data-action="progress"><rect x="15" y="30" height="40" width="300" class="default-btn"></rect><text x="80" y="55" width="200" class="button-text">進捗・コメント登録</text></g>'
          + '<g transform="matrix(1,0,0,1,0,265)" class="btn" data-action="current"><rect x="15" y="30" height="40" width="300" class="default-btn"></rect><text x="90" y="55" width="200" class="button-text">現在学習中に設定</text></g>',
      text: '<text width="[width]" class="get-text get-text-[index]" x="[x]" y="[y]">[text]</text>',
      expandCollapseBtnRadius: 20
    };

    var treeElement = document.getElementById("student-unit-tree");
    var orgChart = new window.getOrgChart(treeElement, {
      theme: "myTheme",
      primaryFields: ["name","isCurrent","progress","progressRate","isActive"],
      photoFields: null,
      linkType: "M",
      enableZoom: true,
      enableSearch: false,
      enableEdit: false,
      enableMove: true,
      enableDetailsView: false,
      enableZoomOnNodeDoubleClick: false,
      secondParentIdField: "secondParentId",
      updatedEvent: function () {
          _this.init();
      },
      dataSource: data,
      renderNodeEvent: this.renderNodeEventHandler,
    });
    this.setState({orgChart:orgChart}, ()=> {_this.init()})
  }

  getNodeByClickedBtn(el) {
    while (el.parentNode) {
      el = el.parentNode;
      if (el.getAttribute("data-node-id"))
        return el;
    }
    return null;
  }

  init() {
    var _this = this;
    var btns = document.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {

      btns[i].addEventListener("click", function () {
        var nodeElement = _this.getNodeByClickedBtn(this);
        var action = this.getAttribute("data-action");
        var id = nodeElement.getAttribute("data-node-id");

        switch (action) {
          case "comment":
            _this.commentModal(id)
            break;
          case "progress":
            _this.props.history.push('/dashboard/progressregister/' + _this.state.studentId + '/' + id)
            break;
          case "current":
            _this.setActive(id)
            break;
          default:
            break;
        }
      });
    }
  }

  renderNodeEventHandler(sender, args) {
    var isActive = args.node.data['isActive'];
    if (isActive) {
      args.content[1] = args.content[1].replace("rect", "rect style='fill: #14d2b8; stroke: #14d2b8;'")
      args.content[1] = args.content[1].replace('<g transform="matrix(1,0,0,1,0,265)" class="btn" data-action="current"><rect x="15" y="30" height="40" width="300" class="default-btn"></rect><text x="90" y="55" width="200" class="button-text">現在学習中に設定</text></g>', '')
    }
  }

  componentDidMount(){
    const { match: { params } } = this.props;
    api.GET('school/unit/relation', {
      studentId:params.studentId
    })
    .then(({data}) => {
      this.setState({data: data.data}, ()=>this.renderChart())
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
    this.setState({studentId: params.studentId})
    this.fetch(params.studentId)
  }

  fetch = (studentId = {}) => {
    this.setState({loading: true})
    api.GET(`school/student/${studentId}`, {})
    .then(({data}) => {
      this.setState({studentInfo: data})
    })
    .catch(err => {
      notification('error', 'エラー', 'データを取得できません。');
    })
  }

  gotoEdit() {
    this.props.history.push(`/dashboard/studentregister/${this.state.studentId}`);
  }

  render() {
    const { selUnit } = this.state
    const { rowStyle, colStyle } = basicStyle;
    return (
      <LayoutContentWrapper>
        <PageHeader>{<IntlMessages id="student.detail.title" />}</PageHeader>        
        <Row style={rowStyle} gutter={0} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box title={<IntlMessages id="student.detail.basictitle" />}>
              <ContentHolder>
                <Row style={rowStyle} gutter={0} justify="start">
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.name" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.familyName} ${this.state.studentInfo.firstName}`}
                  </Col>
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.postnumber" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.postcode}`}
                  </Col>
                </Row>
                <Row style={rowStyle} gutter={0} justify="start">
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.namekana" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.familyKana} ${this.state.studentInfo.firstKana}`}
                  </Col>
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.region" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.prefecture}`}
                  </Col>
                </Row>
                <Row style={rowStyle} gutter={0} justify="start">
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.schoolyear" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.gradeString}`}
                  </Col>
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.city" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.zone}`}
                  </Col>
                </Row>
                <Row style={rowStyle} gutter={0} justify="start">
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.userid" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.loginId}`}
                  </Col>
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.place" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.area}`}
                  </Col>
                </Row>
                <Row style={rowStyle} gutter={0} justify="start">
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.password" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    (------)
                  </Col>
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.address" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.street}`}
                  </Col>
                </Row>
                <Row style={rowStyle} gutter={0} justify="start">
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.registerdate" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.registerDate}`}
                  </Col>
                  <Col md={4} sm={12} xs={12} style={colStyle}>
                    <IntlMessages id="student.detail.building" />
                  </Col>
                  <Col md={8} sm={12} xs={12} style={colStyle}>
                    {this.state.studentInfo && `${this.state.studentInfo.building}`}
                  </Col>
                </Row>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        <Row style={rowStyle} gutter={0} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box title={<IntlMessages id="student.detail.actiontitle" />}>
              <ContentHolder>
                <Button type="primary" onClick={this.gotoEdit.bind(this)}>
                  {<IntlMessages id="student.detail.edit" />}
                </Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        <Row style={rowStyle} gutter={0} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box title={<IntlMessages id="student.detail.progresstitle" />}>
              <ContentHolder>
                <div id="student-unit-tree">
                </div>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        <Modal open={this.state.modalIsOpen} onClose={this.onCloseModal} center classNames={{'closeButton':'modal-close-button'}}>
          <CommentModal comments={selUnit} history={this.props.history} studentId={this.state.studentId}/>
        </Modal>
      </LayoutContentWrapper>
    );
  }
}
