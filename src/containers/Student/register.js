import React, { Component } from 'react';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';
import Box from '../../components/utility/box';
import ContentHolder from '../../components/utility/contentHolder';
import { Row, Col, Input, Select } from 'antd';
import Button from '../../components/uielements/button';
import notification from '../../components/notification';

import basicStyle from '../../settings/basicStyle';
import { customButtonGroup, customButton, customSelect, confirmButton } from './register.style';
import './../../settings/common.scss'
import API from '../../redux/api'

const {Option}  = Select;
const grades = [
  '未就学児',
  '小学1年',
  '小学2年',
  '小学3年',
  '小学4年',
  '小学5年',
  '小学6年',
  '中学1年',
  '中学2年',
  '中学3年',
  '高校1年',
  '高校2年',
  '高校3年',
  '既卒'
]
const prefectures = [
  '北海道',
  '青森県',
  '岩手県',
  '宮城県',
  '秋田県',
  '山形県',
  '福島県',
  '茨城県',
  '栃木県',
  '群馬県',
  "埼玉県",
  "千葉県",
  "東京都",
  "神奈川県",
  "新潟県",
  "富山県",
  "石川県",
  "福井県",
  "山梨県",
  "長野県",
  "岐阜県",
  "静岡県",
  "愛知県",
  "三重県",
  "滋賀県",
  "京都府",
  "大阪府",
  "兵庫県",
  "奈良県",
  "和歌山県",
  "鳥取県",
  "島根県",
  "岡山県",
  "広島県",
  "山口県",
  "徳島県",
  "香川県",
  "愛媛県",
  "高知県",
  "福岡県",
  "佐賀県",
  "長崎県",
  "熊本県",
  "大分県",
  "宮崎県",
  "鹿児島県",
  "沖縄県",
]
export default class extends Component {
  constructor (props) {
    super(props);

    this.state = {
      selId: null,
      is_confirm: false,
      familyName: '',
      firstName: '',
      familyKana: '',
      firstKana: '',
      grade: '',
      loginId: '',
      password: '',
      confirm_password: '',
      postcode: '',
      zone: '',
      area: '',
      street: '',
      building: '',
      prefecture: '',
      join_date: '',
      is_familyName: true,
      is_firstName: true,
      is_familyKana: true,
      is_firstKana: true,
      is_grade: true,
      is_loginId: true,
      is_password: true,
      is_confirm_password: true,
      is_postcode: true,
      is_prefecture: true,
    }
    
    this.confirmHandler = this.confirmHandler.bind(this)
    this.prefectureHandler = this.prefectureHandler.bind(this)
    this.gradeHandler = this.gradeHandler.bind(this)
    this.backHandler = this.backHandler.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
    this.validate = this.validate.bind(this)
    this.save = this.save.bind(this)
  }

  validate() {
    var password = true, confirm_password = true, familyKana =true,
     firstKana = true, familyName = true, firstName = true, postcode = true,
     loginId = true, grade = true, prefecture = true;
    if (this.state.password.length < 6)  password = false
    if (this.state.confirm_password !== this.state.password) confirm_password = false;
    if (this.state.familyName.length < 1) familyName = false
    if (this.state.firstName.length < 1) firstName = false
    if (this.state.loginId.length < 1) loginId = false
    if (this.state.grade.length < 1) grade = false
    if (this.state.prefecture.length < 1) prefecture = false
    postcode = this.state.postcode.match(/^\d{3}-\d{4}$|^\d{7}$/i)
    familyKana = this.state.familyKana.match(/^([ァ-ン])$|^([ｦ-ﾟ])$/i)
    firstKana = this.state.firstKana.match(/^([ァ-ン])$|^([ｦ-ﾟ])$/i)
    if (this.state.password.length === 0 && this.state.selId) password = true
    if (this.state.confirm_password.length === 0 && this.state.selId) confirm_password = true
    var valid = password && confirm_password && familyKana && firstKana && familyName && firstName && postcode && loginId && grade && prefecture
    var join_date = this.state.join_date
    if (join_date === '')
    {
      var today = new Date()
      join_date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    }
    if (loginId) {
      API.GET('auth/checkLoginId', {
        loginId: this.state.loginId
      })
      .then(({data}) => {
        this.setState({
          is_password: password,
          is_confirm_password: confirm_password,
          is_familyKana: familyKana,
          is_familyName: familyName,
          is_firstKana: firstKana,
          is_firstName: firstName,
          is_loginId: data.isAvailable,
          is_postcode: postcode,
          is_grade: grade,
          is_prefecture: prefecture,
          join_date: join_date,
          is_confirm: valid && data.isAvailable
        })
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    } else {
      this.setState({
        is_password: password,
        is_confirm_password: confirm_password,
        is_familyKana: familyKana,
        is_familyName: familyName,
        is_firstKana: firstKana,
        is_firstName: firstName,
        is_loginId: loginId,
        is_postcode: postcode,
        is_grade: grade,
        is_prefecture: prefecture,
        join_date: join_date,
        is_confirm: valid
      })
    }
  }

  save() {
    if (this.state.selId) {
      var data = {
        familyName: this.state.familyName,
        firstName: this.state.firstName,
        familyKana: this.state.familyName,
        firstKana: this.state.firstKana,
        prefecture: this.state.prefecture,
        postcode: this.state.postcode,
        zone: this.state.zone,
        area: this.state.area,
        street: this.state.street,
        building: this.state.building,
        grade: this.state.grade,
        loginId: this.state.loginId,
      }
      if (this.state.password !== '') {
        data['password'] = this.state.password
      }
      API.PUT('school/student/' + this.state.selId, data)
      .then(({data}) => {
        this.props.history.push('/dashboard/student/' + this.state.selId)
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    } else {
      API.POST('school/student', {
        familyName: this.state.familyName,
        firstName: this.state.firstName,
        familyKana: this.state.familyName,
        firstKana: this.state.firstKana,
        prefecture: this.state.prefecture,
        postcode: this.state.postcode,
        zone: this.state.zone,
        area: this.state.area,
        street: this.state.street,
        building: this.state.building,
        grade: this.state.grade,
        loginId: this.state.loginId,
        password: this.state.password
      })
      .then(({data}) => {
        this.props.history.push('/dashboard/studentlist')
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }
  }

  prefectureHandler(value) {
    var valid = value !== ''
    this.setState({prefecture:value, is_prefecture:valid})
  }

  gradeHandler(value) {
    var valid = value !== ''
    this.setState({grade:value, is_grade:valid})
  }

  changeHandler(e) {
    var value = e.target.value;
    var valid = true;
    switch(e.target.name) {
      case 'password':
        if (value.length < 6) valid = false
        if (this.state.selId && value.length === 0) valid = true
        break;
      case 'confirm_password':
        if (value !== this.state.password) valid = false
        if (this.state.selId && value.length === 0) valid = true
        break;
      case 'familyKana':
      case 'firstKana':
        valid = value.match(/[ｧ-ﾝﾞﾟァ-ン0-9]+$/i)
        break;
      case 'postcode':
        valid = value.match(/^\d{3}-\d{4}$|^\d{7}$/i)
        break;
      case 'familyName':
      case 'firstName':
      case 'loginId':
        if (value.length < 1) valid = false
        break;
      default:
        break;
    }
    var state = {}
    state[e.target.name] = value
    state['is_' + e.target.name] = valid

    this.setState(state)
  }

  confirmHandler() {
    this.validate()
  }

  backHandler() {
    this.setState({is_confirm: false})
  }

  componentWillMount() {
    const { match: { params } } = this.props;
    const selId = params.id
    if (selId) {
      API.GET('school/student/' + selId)
      .then(({data}) => {
        this.setState({
          area: data.area,
          building: data.building,
          familyKana: data.familyKana,
          familyName: data.familyName,
          firstKana: data.firstKana,
          firstName: data.firstName,
          loginId: data.loginId,
          grade: data.grade,
          postcode: data.postcode,
          prefecture: data.prefecture,
          join_date: data.registerDate,
          street: data.street,
          selId: data.studentId,
          zone: data.zone,
        })
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }
  }
  componentDidMount() {
  }

  render() {
    const { is_confirm, selId } = this.state
    const { rowStyle, colStyle } = basicStyle;
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <PageHeader>{<IntlMessages id={!selId ? "student.register.registerTitle" : "student.register.changeTitle"} />}</PageHeader>
        <LayoutContent>
        <Row style={rowStyle} gutter={0} justify="start">
            <Col md={24} sm={24} xs={24} style={colStyle}>
              <Box>
                { is_confirm ? 
                <ContentHolder>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.name" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.familyName + ' ' + this.state.firstName}
                    </Col>
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.postnumber" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.postcode}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.namekana" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.familyKana + ' ' + this.state.firstKana}
                    </Col>
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.region" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.prefecture}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.schoolyear" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {grades[this.state.grade]}
                    </Col>
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.city" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.zone}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.userid" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.loginId}
                    </Col>
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.place" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.area}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.password" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      (------)
                    </Col>
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.address" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.street}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.registerdate" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.join_date}
                    </Col>
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <IntlMessages id="student.detail.building" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.building}
                    </Col>
                  </Row>
                  <ContentHolder style={customButtonGroup}>
                    <Button type="secondary" style={customButton} onClick={this.backHandler}>
                      {<IntlMessages id={"register.back"} />}
                    </Button>
                    <Button type="primary" style={customButton} onClick={this.save}>
                      {<IntlMessages id={!selId ? "register.register" : "register.change"} />}
                    </Button>
                  </ContentHolder>
                </ContentHolder> : 
                <ContentHolder>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.name" />
                    </Col>
                      <Col md={3} sm={12} xs={12} style={colStyle}>
                        <Input type="text" name="familyName" className={!this.state.is_familyName ? "input-error": ''} value={this.state.familyName} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                        {!this.state.is_familyName && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                      </Col>
                      <Col md={3} sm={12} xs={12} offset={1} style={colStyle}>
                        <Input type="text" name="firstName" className={!this.state.is_firstName ? "input-error": ''} value={this.state.firstName} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                        {!this.state.is_firstName && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                      </Col>

                    <Col md={4} sm={12} xs={12} offset={1} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.postnumber" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="postcode" className={!this.state.is_postcode ? "input-error": ''} value={this.state.postcode} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_postcode && <span className="error"><IntlMessages id="register.validation.typeError" /></span>}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.namekana" />
                    </Col>
                    <Col md={3} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="familyKana" className={!this.state.is_familyKana ? "input-error": ''} value={this.state.familyKana} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_familyKana && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                    <Col md={3} sm={12} xs={12} offset={1} style={colStyle}>
                      <Input type="text" name="firstKana" className={!this.state.is_firstKana ? "input-error": ''} value={this.state.firstKana} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_firstKana && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>

                    <Col md={4} sm={12} xs={12} offset={1} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.region" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Select name="prefecture" value={this.state.prefecture} className={!this.state.is_prefecture ? "input-error": ''} style={customSelect} onChange={this.prefectureHandler} onBlur={this.prefectureHandler}>
                        {prefectures.map((prefecture, index) => (
                          <Option key={index} value={index}>{prefecture}</Option>
                        ))}
                      </Select>
                      {!this.state.is_prefecture && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.schoolyear" />
                    </Col>
                    <Col md={7} sm={12} xs={12} style={colStyle}>
                      <Select value={this.state.grade} className={!this.state.is_grade ? "input-error": ''} style={customSelect} onChange={this.gradeHandler} onBlur={this.gradeHandler}>
                        {grades.map((grade, index) => (
                          <Option key={index} value={index}>{grade}</Option>
                        ))}
                      </Select>
                      {!this.state.is_grade && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                    <Col md={4} sm={12} xs={12} offset={1} style={colStyle}>
                      <IntlMessages id="student.detail.city" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="zone" value={this.state.zone} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.userid" />
                    </Col>
                    <Col md={7} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="loginId" value={this.state.loginId} className={!this.state.is_loginId ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_loginId && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                    <Col md={4} sm={12} xs={12} offset={1} style={colStyle}>
                      <IntlMessages id="student.detail.place"/>
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="area" value={this.state.area} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.password" />
                    </Col>
                    <Col md={7} sm={12} xs={12} style={colStyle}>
                      <Input type="password" name="password" value={this.state.password} className={!this.state.is_password ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_password && <span className="error"><IntlMessages id="register.validation.passwordLength" /></span>}
                    </Col>
                    <Col md={4} sm={12} xs={12} offset={1} style={colStyle}>
                      <IntlMessages id="student.detail.address" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="street" value={this.state.street} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={4} sm={12} xs={12} style={colStyle}>
                      <span className="red-span">*</span><IntlMessages id="student.detail.confirmpassword" />
                    </Col>
                    <Col md={7} sm={12} xs={12} style={colStyle}>
                      <Input type="password" name="confirm_password" value={this.state.confirm_password} className={!this.state.is_confirm_password ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_confirm_password && <span className="error"><IntlMessages id="register.validation.confirmPassowrd" /></span>}
                    </Col>
                    <Col md={4} sm={12} xs={12} offset={1} style={colStyle}>
                      <IntlMessages id="student.detail.building" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="building" value={this.state.building} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                    </Col>
                  </Row>
                  <ContentHolder>
                    <Button type="primary" style={confirmButton} onClick={this.confirmHandler}>
                      {<IntlMessages id="register.confirm" />}
                    </Button>
                  </ContentHolder>
                </ContentHolder>
              }
              </Box>
            </Col>
          </Row>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
