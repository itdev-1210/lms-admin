import React, { Component } from 'react';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';
import TableWrapper from './antTable.style';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';

import api from '../../redux/api';
import { columns } from './configs';

export default class extends Component {

  state = {
    data: [],
    pagination: {},
    loading: false,
    perPage: 10
  }

  componentDidMount() {
    this.fetch()
  }

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    pager.current = pagination.current;
    this.setState({
      pagination: pager,
    });
    this.fetch({
      perPage: pagination.perPage,
      page: pagination.current,
      sortField: sorter.field,
      sortOrder: sorter.order,
      ...filters,
    });
  };

  fetch = (params = {}) => {
    this.setState({loading: true})
    api.GET('school/student', {
      perPage: 10,
      page: 1,
      ...params
    })
    .then(({data}) => {
      const pagination = {...this.state.pagination}
      pagination.total = data.meta.totalCount;
      pagination.perPage = data.meta.perPage;
      this.setState({
        loading: false,
        data: data.data,
        pagination
      })
    })
    .catch(err => {
      this.setState({
        loading: false
      })
    })
  }

  render() {
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <PageHeader>{<IntlMessages id="student.list.title" />}</PageHeader>
        <LayoutContent>
          <TableWrapper
            rowKey={record => record.studentId}
            columns={columns}
            dataSource={this.state.data}
            loading={this.state.loading}
            pagination={this.state.pagination}
            onChange={this.handleTableChange}
            className="isoSimpleTable"
          />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
