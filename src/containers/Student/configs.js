import React from 'react';
import clone from 'clone';
import IntlMessages from '../../components/utility/intlMessages';
import {
  DateCell,
  ImageCell,
  LinkCell,
  TextCell
} from '../../components/tables/helperCells';

const renderCell = (object, type, key) => {
  const value = object[key];
  switch (type) {
    case 'ImageCell':
      return ImageCell(value);
    case 'DateCell':
      return DateCell(value);
    case 'LinkCell':
      return LinkCell(value, `student/${object["studentId"]}`);
    default:
      return TextCell(value);
  }
};

const columns = [
  {
    title: <IntlMessages id="antTable.title.name" />,
    key: 'firstName',
    width: 200,
    render: object => LinkCell(`${object["familyName"]} ${object["firstName"]}`, `student/${object["studentId"]}`)
  },
  {
    title: <IntlMessages id="antTable.title.namekana" />,
    key: 'firstKana',
    width: 200,
    render: object => TextCell(`${object["familyKana"]} ${object["firstKana"]}`, `student/${object["studentId"]}`)
  },
  {
    title: <IntlMessages id="antTable.title.schoolyear" />,
    key: 'gradeString',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'gradeString')
  },
  {
    title: <IntlMessages id="antTable.title.registerdate" />,
    key: 'registerDate',
    width: 200,
    render: object => renderCell(object, 'DateCell', 'registerDate')
  }
];
const smallColumns = [columns[1], columns[2], columns[3], columns[4]];
const sortColumns = [
  { ...columns[1], sorter: true },
  { ...columns[2], sorter: true },
  { ...columns[3], sorter: true },
  { ...columns[4], sorter: true }
];
const editColumns = [
  { ...columns[1], width: 300 },
  { ...columns[2], width: 300 },
  columns[3],
  columns[4]
];
const groupColumns = [
  columns[0],
  {
    title: 'Name',
    children: [columns[1], columns[2]]
  },
  {
    title: 'Address',
    children: [columns[3], columns[4]]
  }
];
const tableinfos = [
  {
    title: 'Simple Table',
    value: 'simple',
    columns: clone(smallColumns)
  },
  {
    title: 'Sortable Table',
    value: 'sortView',
    columns: clone(sortColumns)
  },
  {
    title: 'Search Text',
    value: 'filterView',
    columns: clone(smallColumns)
  },
  {
    title: 'Editable View',
    value: 'editView',
    columns: clone(editColumns)
  },
  {
    title: 'Grouping View',
    value: 'groupView',
    columns: clone(groupColumns)
  },
  {
    title: 'Customized View',
    value: 'customizedView',
    columns: clone(columns)
  }
];
export { columns, tableinfos };
