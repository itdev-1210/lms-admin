import React, { Component } from 'react';
import './modal.scss'

class CommentModal extends Component {
  render() {
    const {history, comments, studentId} = this.props
    const boldStyle = {
      fontWeight: 'bold'
    }
    return (
      <div className="comment-modal">
        <span className="comment-name">{comments.name}</span>
        <span className="comment-rate">進捗率<span style={boldStyle}>{comments.progressRate}</span></span>
        {comments.progressComment.map((progress, index)=>
        <div className="modal-comment" key={index}>
          <span className="comment-date">{progress.date}</span>
          <span className="comment">{progress.comment}</span>
          <div className="comment-button" onClick={()=>history.push('/dashboard/progressregister/' + studentId + '/' + comments.id + '/' + index)}>{'編集'}</div>
        </div>)}
      </div>
    )
  }
}

export default CommentModal