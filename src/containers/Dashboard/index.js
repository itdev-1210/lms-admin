import React, { Component } from 'react';
import { connect } from 'react-redux';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';
import TableWrapper from './antTable.style';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';

import api from '../../redux/api';
import { columns } from './configs';

class Dashboard extends Component {

  state = {
    data: [],
    loading: false,
  }

  componentDidMount() {
    this.fetch()
  }

  fetch = () => {
    this.setState({loading: true})
    api.GET('school/comment', {
      perPage: 10,
      page: 1
    })
    .then(({data}) => {
      this.setState({
        loading: false,
        data: data.data
      })
    })
    .catch(err => {
      this.setState({
        loading: false
      })
    })
  }

  render() {
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>        
        <PageHeader>{<IntlMessages id="dashboard.title" />}</PageHeader>
        <LayoutContent>
          <h2>直近の進捗(10件)</h2>
          <TableWrapper
            pagination={false}
            rowKey={(record, index) => index}
            columns={columns}
            loading={this.state.loading}
            dataSource={this.state.data}
            className="isoSimpleTable"
          />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}

export default connect(
  null,
)(Dashboard);
