import React from 'react';
import clone from 'clone';
import IntlMessages from '../../components/utility/intlMessages';
import {
  DateCell,
  ImageCell,
  LinkCell,
  TextCell
} from '../../components/tables/helperCells';

const renderCell = (object, type, key) => {
  const value = object[key];
  switch (type) {
    case 'ImageCell':
      return ImageCell(value);
    case 'DateCell':
      return DateCell(value);
    case 'LinkCell':
      return LinkCell(value, 'dashboard/student/1');
    default:
      return TextCell(value);
  }
};

const columns = [
  {
    title: <IntlMessages id="antTable.title.name" />,
    key: 'name',
    width: 200,
    render: object => LinkCell(`${object['student']['familyName']} ${object['student']['firstName']}`, `dashboard/student/${object['studentId']}`)
  },
  {
    title: <IntlMessages id="antTable.title.unitname" />,
    key: 'unitname',
    width: 200,
    render: object => TextCell(`${object['learningUnit']['name']}`)
  },
  {
    title: <IntlMessages id="antTable.title.progress" />,
    key: 'progressRate',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'progressRate')
  },
  {
    title: <IntlMessages id="antTable.title.comment" />,
    key: 'comment',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'comment')
  },
  {
    title: <IntlMessages id="antTable.title.registerdate" />,
    key: 'date',
    width: 200,
    render: object => renderCell(object, 'DateCell', 'date')
  }
];
const smallColumns = [columns[1], columns[2], columns[3], columns[4]];
const sortColumns = [
  { ...columns[1], sorter: true },
  { ...columns[2], sorter: true },
  { ...columns[3], sorter: true },
  { ...columns[4], sorter: true }
];
const editColumns = [
  { ...columns[1], width: 300 },
  { ...columns[2], width: 300 },
  columns[3],
  columns[4]
];
const groupColumns = [
  columns[0],
  {
    title: 'Name',
    children: [columns[1], columns[2]]
  },
  {
    title: 'Address',
    children: [columns[3], columns[4]]
  }
];
const tableinfos = [
  {
    title: 'Simple Table',
    value: 'simple',
    columns: clone(smallColumns)
  },
  {
    title: 'Sortable Table',
    value: 'sortView',
    columns: clone(sortColumns)
  },
  {
    title: 'Search Text',
    value: 'filterView',
    columns: clone(smallColumns)
  },
  {
    title: 'Editable View',
    value: 'editView',
    columns: clone(editColumns)
  },
  {
    title: 'Grouping View',
    value: 'groupView',
    columns: clone(groupColumns)
  },
  {
    title: 'Customized View',
    value: 'customizedView',
    columns: clone(columns)
  }
];
export { columns, tableinfos };
