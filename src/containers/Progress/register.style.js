const customButtonGroup = {
  'display': 'flex',
  'justify-content': 'center',
  'margin': 'auto'
}

const customButton = {
  'margin': '0 10px'
}

const confirmButton = {
  'display': 'flex',
  'margin': 'auto'
}

const customSelect = {
  width: '100%'
}

export { customButtonGroup, customButton, confirmButton, customSelect };