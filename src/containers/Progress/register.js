import React, { Component } from 'react';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';
import Box from '../../components/utility/box';
import ContentHolder from '../../components/utility/contentHolder';
import { Row, Col, Input, Select } from 'antd';
import Button from '../../components/uielements/button';
import notification from '../../components/notification';
import API from '../../redux/api'

import basicStyle from '../../settings/basicStyle';
import { customButton, customSelect } from './register.style';
import TextArea from 'antd/lib/input/TextArea';
import './../../settings/common.scss'
const {Option}  = Select;
export default class extends Component {
  constructor (props) {
    super(props);

    this.state = {
      is_edit: false,
      is_confirm: false,
      datas: [],
      students: [],
      units: [],
      student_name: '',
      unit_name: '',
      selId: null,
      studentId: null,
      unitId: null,
      student: '',
      unit: '',
      rate: 0,
      comment: '',
      join_date: '',
      is_student: true,
      is_unit: true,
      is_rate: true,
      is_comment: true,
    }
    
    this.confirmHandler = this.confirmHandler.bind(this)
    this.backHandler = this.backHandler.bind(this)
    this.studentHandler = this.studentHandler.bind(this)
    this.unitHandler = this.unitHandler.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
    this.save = this.save.bind(this)
  }

  confirmHandler() {
    var student = true, unit = true, rate =true, comment = true
    if (this.state.rate > 100 || this.state.rate < 1)  rate = false
    if (this.state.student.length < 1) student = false
    if (this.state.unit.length < 1) unit = false
    if (this.state.comment.length < 1) comment = false
    var today = new Date()
    var join_date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var valid = student && unit && comment && rate
    var tmp = this.state.students.filter((student) => student.studentId === parseInt(this.state.student))
    this.setState({
      is_student: student,
      is_unit: unit,
      is_rate: rate,
      is_comment: comment,
      join_date: this.state.selId ? this.state.join_date : join_date,
      is_confirm: valid,
      unit_name: this.state.units.filter((unit) => unit.learningUnitId === parseInt(this.state.unit))[0].name,
      student_name: tmp[0].familyName + tmp[0].firstName,
    })
  }
  backHandler() {
    this.setState({is_confirm: false})
  }
  studentHandler(value) {
    var valid = value !== ''
    this.setState({student:value, is_student:valid})
  }
  unitHandler(value) {
    var valid = value !== ''
    this.setState({unit:value, is_unit:valid})
  }
  changeHandler(e) {
    var value = e.target.value;
    var valid = true;
    switch(e.target.name) {
      case 'rate':
        if (value > 100 || value < 1) valid = false
        break;
      case 'comment':
        if (value.length < 1) valid = false
        break;
      default:
        break;
    }
    var state = {}
    state[e.target.name] = value
    state['is_' + e.target.name] = valid

    this.setState(state)
  }
  save() {
    if (this.state.selId) {
      API.PUT('school/comment', {
        studentId: this.state.student,
        learningUnitId: this.state.unit,
        progressRate: this.state.rate,
        comment: this.state.comment,
        date: this.state.join_date,
      })
      .then(({data}) => {
        this.props.history.push('/dashboard/student/' + this.state.student)
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    } else {
      API.POST('school/comment', {
        studentId: this.state.student,
        learningUnitId: this.state.unit,
        progressRate: this.state.rate,
        comment: this.state.comment,
        date: this.state.join_date,
      })
      .then(({data}) => {
        if (this.state.studentId)
          this.props.history.push('/dashboard/student/' + this.state.student)
        else
          this.props.history.push('/dashboard/studentlist')
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }
  }
  componentWillMount() {
    API.GET('school/unit')
    .then(({data}) => {
      this.setState({
        units: data.data,
      })
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
    API.GET('school/student')
    .then(({data}) => {
      this.setState({
        students: data.data,
      })
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
    const { match: { params } } = this.props;
    const { commentId, studentId, learningUnitId } = params
    console.log((typeof studentId) === "undefined")
    this.setState({
      selId: commentId,
      studentId: studentId,
      student: typeof studentId !== "undefined" ? parseInt(studentId) : '',
      unitId: learningUnitId,
      unit: typeof learningUnitId !== "undefined" ? parseInt(learningUnitId) : ''
    })
    if (studentId) {
      API.GET('school/comment', {
        studentId: studentId,
        learningUnitId: learningUnitId
      })
      .then(({data}) => {
        if (data.data.length > 0) {
          if (commentId)
            this.setState({
              rate: data.data[parseInt(commentId)].progressRate,
              join_date: data.data[parseInt(commentId)].date,
              comment: data.data[parseInt(commentId)].comment,
            })
          else
            this.setState({
              rate: data.data[0].progressRate,
            })
        }
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }
  }
  render() {
    const { is_confirm, selId, studentId, unitId } = this.state
    const { rowStyle, colStyle, customCol } = basicStyle;
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <PageHeader>{<IntlMessages id={!selId ? "progress.register.registerTitle" : "progress.register.changeTitle"} />}</PageHeader>
        <LayoutContent>
        <Row style={rowStyle} gutter={0} justify="start">
            <Col md={24} sm={24} xs={24} style={customCol}>
              <Box>
                { is_confirm ? 
                <ContentHolder>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id="progress.register.studentname" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.student_name}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id="progress.register.unit" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.unit_name}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id="progress.register.progress" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.rate}%
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id="progress.register.comment" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.comment}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} offset={9} style={colStyle}>
                      <Button type="secondary" style={customButton} onClick={this.backHandler}>
                        {<IntlMessages id={"register.back"} />}
                      </Button>
                      <Button type="primary" style={customButton} onClick={this.save}>
                        {<IntlMessages id={!selId ? "register.register" : "register.change"} />}
                      </Button>
                    </Col>
                  </Row>
                </ContentHolder> : 
                <ContentHolder>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id="progress.register.studentname" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Select style={customSelect} value={this.state.student} className={!this.state.is_student ? "input-error": ''} onChange={this.studentHandler} onBlur={this.studentHandler} disabled={studentId ? true : false}>
                        {this.state.students.map((student, index) => (
                          <Option key={index} value={student.studentId}>{student.familyName + student.firstName}</Option>
                        ))}
                      </Select>
                      {!this.state.is_student && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>

                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id="progress.register.unit" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Select style={customSelect} value={this.state.unit} className={!this.state.is_unit ? "input-error": ''} onChange={this.unitHandler} onBlur={this.unitHandler} disabled={unitId ? true : false}>
                        {this.state.units.map((unit, index) => (
                          <Option key={index} value={unit.learningUnitId}>{unit.name}</Option>
                        ))}
                      </Select>
                      {!this.state.is_unit && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>

                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id="progress.register.progress" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="number" name="rate" value={this.state.rate} className={!this.state.is_rate ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_rate && <span className="error"><IntlMessages id="register.validation.rate" /></span>}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id="progress.register.comment" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <TextArea name="comment" rows={5} value={this.state.comment} className={!this.state.is_comment ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_comment && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} offset={9} style={colStyle}>
                      <Button type="primary" onClick={this.confirmHandler}>
                        {<IntlMessages id="register.confirm" />}
                      </Button>
                    </Col>
                  </Row>
                </ContentHolder>
              }
              </Box>
            </Col>
          </Row>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
