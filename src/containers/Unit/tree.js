import React, { Component } from 'react';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';
import './tree.scss'
import notification from '../../components/notification';
import API from '../../redux/api'

export default class extends Component {
  constructor(props){
    super(props);
    this.state={
        orgChart: null,
        data: [],
    }
    this.renderChart = this.renderChart.bind(this)
    this.init = this.init.bind(this)
    this.getNodeByClickedBtn = this.getNodeByClickedBtn.bind(this)
  }

  renderChart() {
    var data = this.state.data
    var _this = this;
    if (data.length < 1)
      return;
    var i = 0, j = 0
    for(i=0; i < data.length; i++) {
      if (data[i]['id'] === data[i]['parentId'])
        data[i]['parentId'] = null
      if (i === data.length-1)
        break;
      for (j=i+1; j < data.length; j++) {
        if (data[i].id === data[j].id) {
          data[i]['secondParentId'] = data[j].parentId
          data.splice(j, 1)
        }
      }
    }

    window.getOrgChart.themes.myTheme =
    {
      size: [330, 200],
      toolbarHeight: 0,
      textPointsNoImage: [
          { x: 20, y: 45, width: 300 },
      ],
      box: '<rect x="0" y="0" height="200" width="330" rx="10" ry="10" class="get-box"></rect>'
          + '<g transform="matrix(1,0,0,1,0,85)" class="btn" data-action="detail"><rect x="15" y="30" height="40" width="300" class="default-btn"></rect><text x="140" y="55" width="60" class="button-text">編 集</text></g>',
          // + '<g transform="matrix(1,0,0,1,0,135)" class="btn" data-action="remove"><rect x="15" y="0" height="40" width="300" class="default-btn"></rect><text x="140" y="25" width="60" class="button-text">削 除</text></g>',
      text: '<text width="[width]" class="get-text get-text-[index]" x="[x]" y="[y]">[text]</text>',
      expandCollapseBtnRadius: 20
    };

    var treeElement = document.getElementById("unit-tree");
    var orgChart = new window.getOrgChart(treeElement, {
      theme: "myTheme",
      primaryFields: ["name"],
      photoFields: null,
      linkType: "M",
      enableZoom: true,
      enableSearch: false,
      enableEdit: false,
      enableMove: true,
      enableDetailsView: false,
      enableZoomOnNodeDoubleClick: false,
      secondParentIdField: "secondParentId",
      updatedEvent: function () {
          _this.init();
      },
      dataSource: data,
    });
    this.setState({orgChart:orgChart}, ()=> {_this.init()})
  }

  getNodeByClickedBtn(el) {
    while (el.parentNode) {
      el = el.parentNode;
      if (el.getAttribute("data-node-id"))
        return el;
    }
    return null;
  }
  init() {
    var _this = this;
    var btns = document.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {

      btns[i].addEventListener("click", function () {
        var nodeElement = _this.getNodeByClickedBtn(this);
        var action = this.getAttribute("data-action");
        var id = nodeElement.getAttribute("data-node-id");

        switch (action) {
          case "detail":
            _this.props.history.push('/dashboard/unitregister/' + id)
            break;
          default:
            break;
        }
      });
    }
  }
  componentDidMount(){
    API.GET('school/unit/relation')
    .then(({data}) => {
      this.setState({data: data.data}, ()=>this.renderChart())
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
  }
  componentWillMount() {
  }
  render() {
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <PageHeader>{<IntlMessages id="unit.tree.title" />}</PageHeader>
        <LayoutContent>
          {<IntlMessages id="unit.tree.subTitle" />}
          <div id="unit-tree">
          </div>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
