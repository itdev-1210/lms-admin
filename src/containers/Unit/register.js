import React, { Component } from 'react';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';
import PageHeader from '../../components/utility/pageHeader';
import IntlMessages from '../../components/utility/intlMessages';
import Box from '../../components/utility/box';
import ContentHolder from '../../components/utility/contentHolder';
import { Row, Col, Input, Select, Radio } from 'antd';
import Button from '../../components/uielements/button';

import basicStyle from '../../settings/basicStyle';
import { customButton } from './register.style';
import TextArea from 'antd/lib/input/TextArea';
import RadioGroup from 'antd/lib/radio/group';
import notification from '../../components/notification';
import API from '../../redux/api'
import './register.scss'
import './../../settings/common.scss'

const {Option}  = Select;

const modes = [
  'none',
  'create',
  'interlay',
  'insertRoot',
  'insertLeaf'
]
export default class extends Component {
  constructor (props) {
    super(props);

    this.state = {
      selId: null,
      is_confirm: false,
      units: [],
      unit: '',
      description: '',
      insertMode: -1,
      betweenRootId: '',
      betweenLeafId: '',
      rootId: '',
      leafId: '',
      mode: '',
      is_unit: true,
      is_description: true,
      is_mode: true,
      is_root: true,
      is_leaf: true,
      is_betweenRoot: true,
      is_betweenLeaf: true,
      datas: [],
    }
    
    this.confirmHandler = this.confirmHandler.bind(this)
    this.backHandler = this.backHandler.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
    this.rootHandler = this.rootHandler.bind(this)
    this.leafHandler = this.leafHandler.bind(this)
    this.betweenLeafHandler = this.betweenLeafHandler.bind(this)
    this.betweenRootHandler = this.betweenRootHandler.bind(this)
    this.changeRadio = this.changeRadio.bind(this)
    this.save = this.save.bind(this)
  }

  changeRadio(e) {
    if (e.target.value === 0 || e.target.value === 4)
      this.setState({insertMode: e.target.value, is_root: true, is_leaf: true, is_betweenLeaf: true, is_betweenRoot:true})
    else
      this.setState({insertMode: e.target.value})
  }
  confirmHandler() {
    const {units} = this.state
    var unit = true, description = true, insertMode =true,
      betweenRoot = true, betweenLeaf = true, is_root = true, is_leaf = true, mode = '', tmp='', root_unit = '', leaf_unit = ''
    if (this.state.description.length < 1) description = false
    if (this.state.unit.length < 1) unit = false
    switch(this.state.insertMode) {
      case 1:
        mode = '新たなツリーを作成'
        break;
      case 0:
        mode = '移動しない'
        break;
      case 2:
        if (this.state.betweenRootId === '') {
          insertMode = false
          betweenRoot = false
        }
        if (this.state.betweenLeafId === '') {
          insertMode = false
          betweenLeaf = false
        }
        if (betweenLeaf)
          leaf_unit = units.filter((unit) => unit.learningUnitId === parseInt(this.state.betweenLeafId))[0].name
        if (betweenRoot)
          root_unit = units.filter((unit) => unit.learningUnitId === parseInt(this.state.betweenRootId))[0].name
        tmp = root_unit + 'と' + leaf_unit + 'の間に'
        mode = tmp + (this.state.selId ? '移動' : '挿入')
        break;
      case 3:
        if (this.state.rootId === '') {
          insertMode = false
          is_root = false
        }
        if (is_root)
          root_unit = units.filter((unit) => unit.learningUnitId === parseInt(this.state.rootId))[0].name
        tmp = root_unit + 'の上に新たに'
        mode = tmp + (this.state.selId ? '移動' : '挿入')
        break;
      case 4:
        if (this.state.leafId === '') {
          insertMode = false
          is_leaf = false
        }
        if (is_leaf)
          leaf_unit = units.filter((unit) => unit.learningUnitId === parseInt(this.state.leafId))[0].name
        tmp = leaf_unit + 'の下に新たに'
        mode = tmp + (this.state.selId ? '移動' : '挿入')
        break;
      default:
        insertMode = false
        break;
    }
    var valid = description && unit && insertMode
    this.setState({
      is_description: description,
      is_unit: unit,
      is_mode: insertMode,
      is_root: is_root,
      is_leaf: is_leaf,
      is_betweenLeaf: betweenLeaf,
      is_betweenRoot: betweenRoot,
      mode: mode,
      is_confirm: valid
    })
  }
  save() {
    var data = {
      name: this.state.unit,
      description: this.state.description,
    }
    if (this.state.selId) {
      data['moveMode'] = modes[this.state.insertMode]
      if (this.state.insertMode === 2)
      {
        data['rootLearningUnitId'] = this.state.betweenRootId
        data['leafLearningUnitId'] = this.state.betweenLeafId
      } else if (this.state.insertMode === 3) {
        data['rootLearningUnitId'] = this.state.rootId
      } else if (this.state.insertMode === 4) {
        data['rootLearningUnitId'] = this.state.leafId
      }
      API.PUT('school/unit/' + this.state.selId, data)
      .then(({data}) => {
        this.props.history.push('/dashboard/unitlist')
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    } else {
      data['insertMode'] = modes[this.state.insertMode]
      if (this.state.insertMode === 2)
      {
        data['rootLearningUnitId'] = this.state.betweenRootId
        data['leafLearningUnitId'] = this.state.betweenLeafId
      } else if (this.state.insertMode === 3) {
        data['rootLearningUnitId'] = this.state.rootId
      } else if (this.state.insertMode === 4) {
        data['rootLearningUnitId'] = this.state.leafId
      }
      API.POST('school/unit', data)
      .then(({data}) => {
        this.props.history.push('/dashboard/unitlist')
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }
  }
  backHandler() {
    this.setState({is_confirm: false})
  }
  changeHandler(e) {
    var value = e.target.value;
    var valid = true;
    if (value.length < 1) valid = false
    var state = {}
    state[e.target.name] = value
    state['is_' + e.target.name] = valid

    this.setState(state)
  }
  rootHandler(value) {
    var valid = value !== ''
    this.setState({rootId:value, is_root:valid, insertMode:3, is_leaf: true, is_betweenLeaf: true, is_betweenRoot: true})
  }
  leafHandler(value) {
    var valid = value !== ''
    this.setState({leafId:value, is_leaf:valid, insertMode:4, is_root: true, is_betweenLeaf: true, is_betweenRoot: true})
  }
  betweenLeafHandler(value) {
    var valid = value !== ''
    this.setState({betweenLeafId:value, is_betweenLeaf:valid, insertMode:2, is_leaf: true, is_root: true})
  }
  betweenRootHandler(value) {
    var valid = value !== ''
    this.setState({betweenRootId:value, is_betweenRoot:valid, insertMode:2, is_leaf: true, is_root: true})
  }

  componentWillMount() {
    API.GET('school/unit')
    .then(({data}) => {
      this.setState({
        units: data.data,
      })
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
    const { match: { params } } = this.props;
    const selId = params.id
    if (selId) {
      API.GET('school/unit/' + selId)
      .then(({data}) => {
        this.setState({
          unit: data.name,
          selId: data.learningUnitId,
          description: data.description,
        })
      }).catch(err => {
        notification('error', 'エラー', '無効な認証');
      })
    }
  }
  render() {
    const { is_confirm, selId, units } = this.state
    const { rowStyle, colStyle, customCol } = basicStyle;
    const radioStyle = {
      display: 'flex',
      margin: '10px 0px'
    };
    const nameStyle = {
      width: '200px',
    }
    const paddingStyle = {
      padding: '0px 5px',
    }
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <PageHeader>{<IntlMessages id={!selId ? "unit.register.registerTitle" : "unit.register.changeTitle"} />}</PageHeader>
        <LayoutContent>
        <Row style={rowStyle} gutter={0} justify="start">
            <Col md={24} sm={24} xs={24} style={customCol}>
              <Box>
                { is_confirm ? 
                <ContentHolder>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id="unit.register.name" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.unit}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id="unit.register.description" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.description}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <IntlMessages id={!selId ? "unit.register.insert" : "unit.register.move"} />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      {this.state.mode}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} offset={9} style={colStyle}>
                      <Button type="secondary" style={customButton} onClick={this.backHandler}>
                        {<IntlMessages id={"register.back"} />}
                      </Button>
                      <Button type="primary" style={customButton} onClick={this.save}>
                        {<IntlMessages id={!selId ? "register.register" : "register.change"} />}
                      </Button>
                    </Col>
                  </Row>
                </ContentHolder> : 
                <ContentHolder>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id="unit.register.name" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <Input type="text" name="unit" value={this.state.unit} className={!this.state.is_unit ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_unit && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>

                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id="unit.register.description" />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <TextArea rows={5} name="description" value={this.state.description} className={!this.state.is_description ? "input-error": ''} onChange={this.changeHandler} onBlur={this.changeHandler}/>
                      {!this.state.is_description && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>

                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} style={customCol}>
                      <span className="red-span">*</span><IntlMessages id={!selId ? "unit.register.insert" : "unit.register.move"} />
                    </Col>
                    <Col md={8} sm={12} xs={12} style={colStyle}>
                      <RadioGroup onChange={this.changeRadio} value={this.state.insertMode}>
                        <Radio style={radioStyle} value={1}>
                          <IntlMessages id={!selId ? "unit.register.method.create" : "unit.register.method.move"} />
                        </Radio>
                        <Radio style={radioStyle} value={2}>
                          <Select style={nameStyle} value={this.state.betweenRootId} className={!this.state.is_betweenRoot ? "input-error": ''} onChange={this.betweenRootHandler} onBlur={this.betweenRootHandler}>
                            {units.map((unit, index) => (
                              <Option key={index} value={unit.learningUnitId}>{unit.name}</Option>
                            ))}
                          </Select>
                          <span style={paddingStyle}><IntlMessages id="unit.register.method.of" /></span>
                          <Select style={nameStyle} value={this.state.betweenLeafId} className={!this.state.is_betweenLeaf ? "input-error": ''} onChange={this.betweenLeafHandler} onBlur={this.betweenLeafHandler}>
                            {units.map((unit, index) => (
                              <Option key={index} value={unit.learningUnitId}>{unit.name}</Option>
                            ))}
                          </Select>
                          <span style={paddingStyle}><IntlMessages id={!selId ? "unit.register.method.insertbetween" : "unit.register.method.movebetween"} /></span>
                        </Radio>
                        <Radio style={radioStyle} value={3}>
                          <Select style={nameStyle} value={this.state.rootId} className={!this.state.is_root ? "input-error": ''} onChange={this.rootHandler} onBlur={this.rootHandler}>
                            {units.map((unit, index) => (
                              <Option key={index} value={unit.learningUnitId}>{unit.name}</Option>
                            ))}
                          </Select>
                          <span style={paddingStyle}><IntlMessages id={!selId ? "unit.register.method.insertabove" : "unit.register.method.moveabove"} /></span>
                        </Radio>
                        <Radio style={radioStyle} value={4}>
                          <Select style={nameStyle} value={this.state.leafId} className={!this.state.is_leaf ? "input-error": ''} onChange={this.leafHandler} onBlur={this.leafHandler}>
                            {units.map((unit, index) => (
                              <Option key={index} value={unit.learningUnitId}>{unit.name}</Option>
                            ))}
                          </Select>
                          <span style={paddingStyle}><IntlMessages id={!selId ? "unit.register.method.insertbelow" : "unit.register.method.movebelow"} /></span>
                        </Radio>
                        {selId &&
                        <Radio style={radioStyle} value={0}>
                          <IntlMessages id={"unit.register.method.nomove"} />
                        </Radio>
                        }
                      </RadioGroup>
                      {!this.state.is_mode && <span className="error"><IntlMessages id="register.validation.required" /></span>}
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={0} justify="start">
                    <Col md={8} sm={12} xs={12} offset={9} style={colStyle}>
                      <Button type="primary" onClick={this.confirmHandler}>
                        {<IntlMessages id="register.confirm" />}
                      </Button>
                    </Col>
                  </Row>
                </ContentHolder>
              }
              </Box>
            </Col>
          </Row>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
