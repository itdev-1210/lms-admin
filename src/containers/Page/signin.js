import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Input from '../../components/uielements/input';
import notification from '../../components/notification';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import IntlMessages from '../../components/utility/intlMessages';
import SignInStyleWrapper from './signin.style';
import api from '../../redux/api';

const { login } = authAction;

class SignIn extends Component {
  state = {
    redirectToReferrer: false,
    loginId: '',
    password: ''
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  handleLogin = () => {
    const { login } = this.props; 
    api.POST('auth/login', {
      loginId: this.state.loginId,
      password: this.state.password
    }).then(({data}) => {
      if (data.accessToken&& data.type === 'school') {
        login({
          accessToken: data.accessToken,
          profile: data
        })
        this.props.history.push('/dashboard');
      }else{
        notification('error', 'エラー', '無効な認証');
      }
    }).catch(err => {
      notification('error', 'エラー', '無効な認証');
    })
  };
  render() {
    const from = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <SignInStyleWrapper className="isoSignInPage">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signInTitle" />
              </Link>
            </div>

            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Input type="text" size="large" placeholder="ユーザID" value={this.state.loginId} onChange={event => this.setState({loginId: event.target.value})}/>
              </div>

              <div className="isoInputWrapper">
                <Input size="large" type="password" placeholder="パスワード" value={this.state.password} onChange={event => this.setState({password: event.target.value})}/>
              </div>

              <div className="isoInputWrapper isoLeftRightComponent">
                <Button type="primary" onClick={this.handleLogin}>
                  <IntlMessages id="page.signInButton" />
                </Button>
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false
  }),
  { login }
)(SignIn);
