import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import asyncComponent from '../../helpers/AsyncFunc';

const routes = [
  {
    path: '',
    component: asyncComponent(() => import('../Dashboard')),
  },
  {
    path: 'studentlist',
    component: asyncComponent(() => import('../Student')),
  },
  {
    path: 'student/:studentId',
    component: asyncComponent(() => import('../Student/detail')),
  },
  {
    path: 'studentregister',
    component: asyncComponent(() => import('../Student/register')),
  },
  {
    path: 'studentregister/:id',
    component: asyncComponent(() => import('../Student/register')),
  },
  {
    path: 'progressregister',
    component: asyncComponent(() => import('../Progress/register')),
  },
  {
    path: 'progressregister/:studentId/:learningUnitId',
    component: asyncComponent(() => import('../Progress/register')),
  },
  {
    path: 'progressregister/:studentId/:learningUnitId/:commentId',
    component: asyncComponent(() => import('../Progress/register')),
  },
  {
    path: 'unitlist',
    component: asyncComponent(() => import('../Unit')),
  },
  {
    path: 'unittree',
    component: asyncComponent(() => import('../Unit/tree')),
  },
  {
    path: 'unitregister',
    component: asyncComponent(() => import('../Unit/register')),
  },
  {
    path: 'unitregister/:id',
    component: asyncComponent(() => import('../Unit/register')),
  },
];

class AppRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default AppRouter;
