const options = [
  {
    key: 'studentmenu',
    label: 'sidebar.studentmenu',
    leftIcon: 'ion-person',
    children:[
      {
        key: 'studentlist',
        label: 'sidebar.studentlist'
      },
      {
        key: 'studentregister',
        label: 'sidebar.studentregister'
      },
      {
        key: 'progressregister',
        label: 'sidebar.progressregister'
      }
    ]
  },
  {
    key: 'unitmenu',
    label: 'sidebar.unitmenu',
    leftIcon: 'ion-document',
    children:[
      {
        key: 'unittree',
        label: 'sidebar.unittree'
      },
      {
        key: 'unitlist',
        label: 'sidebar.unitlist'
      },
      {
        key: 'unitregister',
        label: 'sidebar.unitregister'
      }
    ]
  },
];
export default options;
