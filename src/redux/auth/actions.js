const actions = {
  CHECK_AUTHORIZATION: 'CHECK_AUTHORIZATION',
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  FETCH_PROFILE: 'FETCH_PROFILE',
  PROFILE_SUCCESS: 'PROFILE_SUCCESS',
  LOGOUT: 'LOGOUT',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_ERROR: 'LOGIN_ERROR',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  login: (payload) => ({
    type: actions.LOGIN_REQUEST,
    payload: payload
  }),
  getProfile: () => ({
    type: actions.FETCH_PROFILE
  }),  
  logout: () => ({
    type: actions.LOGOUT
  })
};
export default actions;
